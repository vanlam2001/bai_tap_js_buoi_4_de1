/*
bài 1
Sắp xếp 3 số theo thứ tự tăng dần

Bước 1: Tạo biến num1 num2 num3
Bước 2: Sắp xếp cho số 3 số thứ tự tăng dần từ nhỏ đến lớn
Bước 3: In kết quả theo biểu mẫu

Kết quả số thứ tự tăng từ nhỏ đến lớn

*/

document.getElementById("sortNumber").onclick = function () {
    var num1 = document.getElementById("Num1").value * 1;
    num2 = document.getElementById("Num2").value * 1;
    num3 = document.getElementById("Num3").value * 1;

    if (num1 >= num2 && num1 >= num3) {
        if (num2 > num3 || num2 === num3) {
            document.getElementById('txtSortNumber').innerHTML = `${num3} ${num2} ${num1}`;
        } else {
            document.getElementById('txtSortNumber').innerHTML = `${num2} ${num3}  ${num1}`;
        }
    } else if (num2 >= num3 && num2 >= num1) {
        if (num1 > num3 || num1 === num3) {
            document.getElementById('txtSortNumber').innerHTML = ` ${num3} ${num1}  ${num2}`;
        } else {
            document.getElementById('txtSortNumber').innerHTML = ` ${num1}  ${num3}  ${num2}`;
        }
    } else if (num3 >= num1 && num3 >= num2) {
        if (num1 > num2 || num1 === num2) {
            document.getElementById('txtSortNumber').innerHTML = `${num2} ${num1}  ${num3} `;
        } else {
            document.getElementById('txtSortNumber').innerHTML = ` ${num1} ${num2}  ${num3} `;
        }
    }
}



/*
Bài 2
Chương trình "Chào hỏi"

Bước 1: Tạo biến user, hello
Bước 2: Tạo để người dùng chọn thành viên cần gửi lời chào
Bước 3: In kết quả theo biễu mẫu

Kết quả lời chào đúng thành viên đã liệt kê
*/

document.getElementById("sayHello").onclick = function () {
    var user = document.getElementById("user").value,
        hello = document.getElementById("txtHello");
    hello.innerHTML = "B" == user ? "Xin chào Ba!" : "M" == user ? "Xin chào Mẹ!" : "A" == user ? "Xin chào Anh Trai!" : "E" == user ? "Xin chào Em gái!" : "Xin chào Người lạ ơi!"
}

/*
Bài 3
Đếm số chẵn lẻ

Bước 1: Tạo biến so1, so2, so3, sochan va sole
Bước 2: Sắp xếp theo chẵn ra chẵn lẻ ra lẻ
Bước 3: In kết quả theo biểu mẫu

Kết quả chẵn lẻ bao nhiêu kết quả bấy nhiêu
*/

document.getElementById("count").onclick = function () {
    var so1 = document.getElementById("count1").value * 1;
    so2 = document.getElementById("count2").value * 1;
    so3 = document.getElementById("count3").value * 1;
    sochan = 0;
    sole = 0;

    if (so1 <= 0 || so2 <= 0 || so3 <= 0) {
        document.getElementById("txtcount").innerHTML = "", alert("Dữ liệu không hợp lệ")
    } else {
        if (so1 % 2 == 0) {
            sochan++;

        }

        if (so2 % 2 == 0) {
            sochan++;
        }

        if (so3 % 2 == 0) {
            sochan++;
        }
        sole = 3 - sochan;
        document.getElementById("txtcount").innerHTML = `<p>Số lượng số chẵn: ${sochan}</p>` + `<p>Số lượng số lẻ ${sole}</p>`;

    }

}


/*
Bài 4
Đoán hình tam giác

Bước 1: Tạo biến canh1 canh2 canh3 và ketQua
Bước 2: Sử dụng công thức xác định các loại tam giác  (vuông, đều, cân), các tam giác còn lại xếp loại là tam giác khác
Bước 3: In kết quả theo biễu mẫu

Xếp loại theo tam giác
*/

document.getElementById('btnEdge').onclick = function () {
    var canh1 = document.getElementById('canh1').value * 1;
    var canh2 = document.getElementById('canh2').value * 1;
    var canh3 = document.getElementById('canh3').value * 1;
    ketQua = '';
    if (canh1 <= 0 && canh2 <= 0 && canh3 <= 0) {
        document.getElementById("txtEdge").innerHTML = "", alert("Dữ liệu không hợp lệ")
    }
    else {
        if (canh1 > 0 && canh2 > 0 && canh3 > 0) {
            if (canh1 === canh2 && canh2 === canh3) {
                ketQua = 'Tam giác đều'
            }
            else if (canh1 === canh2 || canh1 === canh3 || canh2 === canh3) {
                ketQua = 'tam giác cân';
            }
            else if (canh1 * canh1 === canh2 * canh2 + canh3 * canh3 || canh2 * canh2 === canh1 * canh1 + canh3 * canh3 || canh3 * canh3 === canh1 * canh1 + canh2 * canh2) {
                ketQua = 'tam giác vuông'
            }
            else {
                ketQua = 'loại tam giác khác'
            }
        }
    }


    if (canh1 + canh2 > canh3 && canh1 + canh3 > canh2 && canh2 + canh3 > canh1) {
        test = false;
    } else {
        ketQua = 'không hợp lệ'
    }

    document.getElementById('txtEdge').innerHTML = `${ketQua}`;

}




